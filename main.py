import flask
from flask import request, render_template

import os
import json
import argparse
import requests
import pygsheets

from format_sheet import format_sheet, create_doc

import google.oauth2.credentials
import google_auth_oauthlib.flow
from googleapiclient.discovery import build

parser = argparse.ArgumentParser()
parser.add_argument("--port", type=int, default=8080)
args = parser.parse_args()


SCOPES = ['https://www.googleapis.com/auth/spreadsheets',
        'https://www.googleapis.com/auth/drive.metadata.readonly',
        'https://www.googleapis.com/auth/drive']

API_SERVICE_NAME = 'drive'
API_VERSION = 'v4'

app = flask.Flask(__name__)
app.secret_key = 'secret'


@app.route('/')
def index():
    if 'credentials' not in flask.session:
        return flask.redirect('authorize')

    return render_template('index.html')


@app.route('/format')
def format():
    if 'credentials' not in flask.session:
        return flask.redirect('authorize')

    doc_url = None
    sheet_url = request.args.get('sheet_url')

    # Load credentials from the session.
    credentials = google.oauth2.credentials.Credentials(
    **flask.session['credentials'])

    gc = pygsheets.authorize(custom_credentials=credentials)

    try:
        sh = gc.open_by_url(sheet_url)
    except pygsheets.exceptions.NoValidUrlKeyFound:
        return render_template('error.html', msg='The spreadsheet doesn\'t exist, did you input the right URL ?')

    # Format values and create the result worksheet
    formatted_values = format_sheet(sh)

    # Create the Google Doc
    if request.args.get('create_doc') == 'on':
        service = build('docs', 'v1', credentials=credentials)
        doc = create_doc(credentials, service, formatted_values)
        doc_url = 'https://docs.google.com/document/d/' + doc.get('documentId')


    # Save credentials back to session in case access token was refreshed.
    flask.session['credentials'] = credentials_to_dict(credentials)

    return render_template('format.html',
            sheet_title=sh.title,
            results_url=sh.worksheet('title','Results').url,
            doc_url=doc_url)


@app.route('/authorize')
def authorize():
    flow = google_auth_oauthlib.flow.Flow.from_client_config(
          json.loads(os.environ['GOOGLE_CLIENT_SECRETS']), scopes=SCOPES)

    flow.redirect_uri = flask.url_for('oauth2callback', _external=True)

    authorization_url, state = flow.authorization_url(
          access_type='offline',
          include_granted_scopes='true')

    # Store the state so the callback can verify the auth server response.
    flask.session['state'] = state

    return flask.redirect(authorization_url)


@app.route('/oauth2callback')
def oauth2callback():
    state = flask.session['state']

    flow = google_auth_oauthlib.flow.Flow.from_client_config(
          json.loads(os.environ['GOOGLE_CLIENT_SECRETS']), scopes=SCOPES, state=state)
    flow.redirect_uri = flask.url_for('oauth2callback', _external=True)

    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = flask.request.url
    flow.fetch_token(authorization_response=authorization_response)

    # Store credentials in the session.
    credentials = flow.credentials
    flask.session['credentials'] = credentials_to_dict(credentials)

    return flask.redirect(flask.url_for('index'))


@app.route('/revoke')
def revoke():
    if 'credentials' not in flask.session:
        return 'You need to <a href="/authorize">authorize</a> before ' + \
                'testing the code to revoke credentials.'

    credentials = google.oauth2.credentials.Credentials(
            **flask.session['credentials'])

    revoke = requests.post('https://oauth2.googleapis.com/revoke',
            params={'token': credentials.token},
            headers = {'content-type': 'application/x-www-form-urlencoded'})

    status_code = getattr(revoke, 'status_code')

    if status_code == 200:
        return 'Credentials successfully revoked.'
    else:
        return 'An error occurred.'


@app.route('/clear')
def clear_credentials():
    if 'credentials' in flask.session:
        del flask.session['credentials']
    return 'Credentials have been cleared.'


def credentials_to_dict(credentials):
    return {'token': credentials.token,
          'refresh_token': credentials.refresh_token,
          'token_uri': credentials.token_uri,
          'client_id': credentials.client_id,
          'client_secret': credentials.client_secret,
          'scopes': credentials.scopes}


if __name__ == '__main__':
    # When running locally, disable OAuthlib's HTTPs verification.
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    app.run(host="0.0.0.0", port=args.port)
