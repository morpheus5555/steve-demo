import numpy as np
from math import ceil
from gdoctableapppy import gdoctableapp

# Add a worksheet in the spreadsheet `sheet` containing formatted data
def format_sheet(sheet):
    wks = sheet.sheet1

    sheet_values = wks.get_values('A1', 'C'+str(wks.rows))

    # Matrix of formatted values
    formatted_values = [
        ['\n'.join(parse_values(values)).strip()] for values in sheet_values
    ]

    result_sheet = get_result_worksheet(sheet)
    result_sheet.clear()
    result_sheet.update_row(1, formatted_values)

    return formatted_values

# Get `Results` worksheet, and create it if it doesn't exist
def get_result_worksheet(sheet):
    try:
        result_sheet = sheet.worksheet('title', 'Results')
    except:
        result_sheet = False

    return result_sheet or sheet.add_worksheet('Results', rows=sheet.sheet1.rows, cols=sheet.sheet1.cols)

def parse_values(values):
    # Separate values if they are inside the same cell
    values = [val for value in values for val in value.strip().split('\n')][0:3]

    # Set `Customer` if name is not defined
    values = ['Customer' if len(values[0]) <= 0 else values[0]] + values[1:]

    # Remove empty values
    return [ value for value in values if len(value) > 0]

# Create a document named `Results` and insert the data table
def create_doc(credentials, service, formatted_values):
    body = {
        'title': 'Results'
    }

    # Create document
    doc = service.documents().create(body=body).execute()

    nrows = ceil(len(formatted_values)/3)

    # Fill missing cells
    table_values = [value[0] for value in formatted_values]+(['']*(nrows*3-len(formatted_values)))

    # 1D to 2D array
    table = np.reshape(table_values, (-1,3)).tolist()

    resource = {
        "oauth2": credentials,
        "documentId": doc.get('documentId'),
        "rows": nrows,
        "columns": 3,
        "createIndex": 1,
        "values": table
    }

    # Create the table inside the document
    gdoctableapp.CreateTable(resource)

    # Vertical align in cells
    requests = [{
        'updateTableCellStyle': {
            'tableCellStyle': {
                'contentAlignment': 3
            },
            'fields': '*',
            'tableStartLocation': {
                'index': 2
            }
        }
    }]

    service.documents().batchUpdate(documentId=doc.get('documentId'), body={'requests': requests}).execute()

    return doc
